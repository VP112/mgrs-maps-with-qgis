# MGRS maps with QGIS

Dieses Projekt enthält topografische Karten, die mit dem Open-Source-Programm [QGIS 3.10 LTR](https://www.qgis.org/) geöffnet und angepasst werden können. Die Karten sind auf Lesbarkeit und den Einsatz bei Rettungskräften optimiert. Über den druckbaren Kartenausschnitten liegt ein [MGRS-/UTMREF-Raster](https://de.wikipedia.org/wiki/UTM-Referenzsystem) zur Positions- bzw. Koordinatenbestimmung.

![32ULV9558796250](./assets/readme/map-example.png "Beispielkarte 32ULV9558796250 - 1:50000")

*[Karte als PDF in Originalgröße (DIN A4) herunterladen](https://gitlab.com/VP112/mgrs-maps-with-qgis/-/raw/master/assets/readme/32ULV9558796250_1_50000_A4.pdf?inline=false)*

Bei Zugriff auf eine OSM-PostGIS-Datenbank können POIs (Points of interest) eingeblendet werden.

![32UMA4008034255](./assets/readme/map-poi-example.png "Beispielkarte 32UMA4008034255 mit POIs - 1:50000")

*[Karte als PDF in Originalgröße (DIN A4) herunterladen](https://gitlab.com/VP112/mgrs-maps-with-qgis/-/raw/master/assets/readme/32UMA4008034255_1_50000_A4.pdf?inline=false)*

## ACHTUNG!

Bevor eine Karte in QGIS geöffnet wird, muss die Datei [layoutfunctions.py](./assets/python/expressions/layoutfunctions.py) zwingend in folgendes Verzeichnis kopiert werden:

* Linux

		~/.local/share/QGIS/QGIS3/profiles/default/python/expressions/

* Windows

		C:\Benutzer\<Benutzername>\AppData\Roaming\QGIS\QGIS3\profiles\default\python\expressions

## Die Karten

Das Projektverzeichnis enthält die beiden Unterverzeichnisse *maps-OpenTopoMap* und *maps-localhost*.

* **[maps-OpenTopoMap](./maps-OpenTopoMap/)**
	- Hier liegen Karten, die auf jedem Rechner geöffnet werden können, der mit dem Internet verbunden ist.
	- Diese Karten verwenden den Kachel-Server von [OpenTopoMap](https://opentopomap.org/#map=5/49.000/10.000), der unter der Adresse `https://tile.opentopomap.org/{z}/{x}/{y}.png` erreichbar ist.

* **[maps-localhost](./maps-localhost/)**
	- Hier liegen Karten, die auf einem Rechner geöffnet werden können, auf dem ein unter der Adresse `http://localhost/hot/{z}/{x}/{y}.png` erreichbarer Kachel-Server läuft.
	- Ist der Kachel-Server unter einer anderen Adresse erreichbar, weil er z.B. in einem Netzwerk läuft, können die qgs-Dateien leicht angepasst werden. Dazu muss lediglich der zwei mal in jeder qgs-Datei enthaltenen Pfad `http://localhost/hot/` durch den neuen Pfad ersetzt werden.

Aktuell existiert für jedes deutsche Bundesland mindestens eine QGIS-Datei. Diese Dateien sind entsprechend der UTM-Zone, in der das jeweilige Bundesland liegt, auf die Unterverzeichnisse (*UTM-31*,) *UTM-32* und *UTM-33* verteilt. Eine QGIS-Karte, die mehrere UTM-Zonen abdeckt, ist aus technischen Gründen nicht möglich. Die UTM-Zonengrenzen sind auf den Karten deutlich hervorgehoben und müssen unbedingt beachtet werden, da das über der Karte liegende Raster nur für die UTM-Zone gilt, für die die Karte erstellt wurde!

Jede QGIS-Datei enthält folgende Layouts, deren Kartenausschnitt nach Bedarf verschoben werden kann:

* 1:100000 mit 5km-Raster, DIN A3 hoch
* 1:100000 mit 5km-Raster, DIN A3 quer
* 1:100000 mit 5km-Raster, DIN A4 hoch
* 1:100000 mit 5km-Raster, DIN A4 quer
* 1:50000 mit 1km-Raster, DIN A3 hoch
* 1:50000 mit 1km-Raster, DIN A3 quer
* 1:50000 mit 1km-Raster, DIN A4 hoch
* 1:50000 mit 1km-Raster, DIN A4 quer
* 1:25000 mit 1km-Raster, DIN A3 hoch
* 1:25000 mit 1km-Raster, DIN A3 quer
* 1:25000 mit 1km-Raster, DIN A4 hoch
* 1:25000 mit 1km-Raster, DIN A4 quer

Auf den Layouts ist unten rechts ein THW-Logo eingeblendet. Wer dieses ersetzen möchte, kann die Verzeichnisstruktur des Projekts auf den eigenen Rechner übernehmen und die Datei `logo.svg` im Verzeichnis `assets\images\` austauschen.

## QGIS

Nachdem man in QGIS eine qgs-Datei geöffnet hat, wird die zugehörige Karte im Hauptfenster geladen. In der Regel ist der Kartenausschnitt auf die Landeshauptstadt zentriert. Erstreckt sich ein Bundesland über mehrere UTM-Zonen, gibt es je UTM-Zone eine qgs-Datei. Liegt die Landeshauptstadt in einer anderen UTM-Zone, ist die Karte auf die größte Stadt der aktuellen UTM-Zone zentriert.

Werden beim ersten Öffnen einer Karte nicht alle Kacheln geladen, kann die Karte mittels *F5* nochmal aktualisiert werden.

In der Hauptansicht von QGIS sind folgende Plugins hilfreich, die über die Menüfunktion '*Erweiterungen*' / '*Erweiterungen verwalten und installieren...*' installiert werden können:

* **[MGRS Tools](https://github.com/NationalSecurityAgency/qgis-mgrs-plugin/#readme)**, womit die MGRS-Koordinate jedes Punkts auf der Karte ermittet und womit nach Eingabe einer MGRS-Koordinate deren Position auf der Karte angezeigt werden kann.

* **[Lat Lon Tools](https://github.com/NationalSecurityAgency/qgis-latlontools-plugin/#readme)**, womit eine MGRS-Koordinate in 10 andere Koordinatensysteme  oder eine Koordinate aus 10 verschiedenen Systemen in eine MGRS-Koordinate konvertiert werden kann.

Die vorangelegten, druckbaren Karten-Layouts mit MGRS-Raster erreicht man über die Layout-Verwaltung, die man über die Menüfunktion '*Projekt*' / '*Layout-Verwaltung...*' oder dieses Symbol öffnen kann:

![Layouts öffnen](./assets/readme/de-button-layout.png "Layouts öffnen")

In der Layout-Verwaltung wählt man ein oder mehrere Layouts aus und öffnet diese über den Knopf '*Anzeigen*':

![Layout-Verwaltung](./assets/readme/de-popup-layout.png "Layout-Verwaltung")

Die ausgewählten Layouts öffnen sich in separaten Fenstern. Auch hier gilt: Werden nicht alle Kacheln geladen, kann die Karte mittels *F5* nochmal aktualisiert werden.

Bevor man den angezeigten Kartenausschnitt verschieben kann, muss das Werkzeug '*Inhalt verschieben*' über die Menüfunktion '*Bearbeiten*' / '*Inhalt verschieben*' oder dieses Symbol aktiviert werden:

![Inhalt verschieben](./assets/readme/de-button-move.png "Inhalt verschieben")

## Points of interest

Bei Zugriff auf eine OSM-PostGIS-Datenbank können POIs (Points of interest) eingeblendet werden. Je [maps-localhost-Karte](./maps-localhost/) stehen verschiedene Layer zur Verfügung, die auch als einzelne [Layer-Definition-Files](./layer-definitions) heruntergeladen werden können. Diese POI-Layer zeigen farbige Punkte auf der Karte an, wenn folgende Kriterien für OSM-Punkte (nodes) oder OSM-Flächen (areas) erfüllt werden:

* Feuerwehr (rot)

		"amenity" = 'fire_station'

* Krankenhaus (weiß)

		"amenity" = 'hospital'

* Polizei (grün)

		"amenity" = 'police'

* Rathaus/Verwaltung (hellblau)

		"amenity" = 'townhall' OR "building" = 'townhall'

* THW (blau)

		"amenity" = 'emergency_service' AND "operator" LIKE '%THW%'

## Bekannte Einschränkungen und Fehler

* Das MGRS-Raster eines Karten-Layouts kann nur für **eine** UTM-Zone definiert werden. Somit ist eine Karte immer nur für die UTM-Zone gültig, für die das Layout erstellt wurde. Die UTM-Zonengrenzen sind auf den Karten deutlich hervorgehoben und müssen unbedingt beachtet werden!

* Auf dem Karten-Layout wird oben links die UTM-Zone (z.B. *32*) und das UTM-Band (z.B. *U*) angezeigt. Die Ermittlung des UTM-Bands ist noch fehlerhaft und springt nicht genau an der Bandgrenze auf den nächsten Wert um. So wird auf der Bayern-Karte für die Stadt München der Wert *32T* anzeigt, obwohl der komplette Kartenausschnitt noch im Band *32U* liegt und die Grenze zum Band *T* einige Kilometer südlich des Kartenausschnitts verläuft.

* Es werden nur die POIs angezeigt, die die oben aufgelisteten Kriterien erfüllen. Ist eine Einrichtung in der OSM-PostGIS-Datenbank gar nicht oder mit anderen Schlüssel-Wert-Kombinationen hinterlegt, taucht sie auch nicht als POI auf der Karte auf. Das betrifft vor allem Einrichtungen des THW.

## Quellen

### Verwendete Software

* [Apache HTTP Server](https://en.wikipedia.org/wiki/Apache_HTTP_Server) — A free and open-source cross-platform web server software.

* [GDAL](https://gdal.org/) — A translator library for raster and vector geospatial data formats.

* [Mapnik](https://mapnik.org/) — An open-source mapping toolkit for desktop and server based map rendering.

* [mod_tile](https://wiki.openstreetmap.org/wiki/Mod_tile) — A system to serve raster tiles for example to use within a slippy map. It provides a dynamic combination of efficient caching and on the fly rendering.

* [osm2pgsql](https://wiki.openstreetmap.org/wiki/Osm2pgsql) — A command-line based program that converts OpenStreetMap data to postGIS-enabled PostgreSQL databases.

* [phyghtmap](http://katze.tfiu.de/projects/phyghtmap/) — A little program which lets you easily generate OSM contour lines from NASA SRTM data.

* [PostgreSQL](https://www.postgresql.org/) — A free and open-source relational database management system (RDBMS) emphasizing extensibility and SQL compliance.

* [QGIS](https://www.qgis.org/) — A free and open-source cross-platform desktop geographic information system (GIS) application that supports viewing, editing, and analysis of geospatial data.

* [renderd](https://www.volkerschatz.com/net/osm/renderd.html) — A rendering daemon for rendering OpenStreetMap tiles.

### Datenquellen

* [OpenStreetMap](https://www.openstreetmap.org/about) — Kartendaten

* [OpenTopoMap](https://opentopomap.org/about) — Kartendarstellung

* [Shuttle Radar Topography Mission (SRTM)](https://www2.jpl.nasa.gov/srtm/) — Höhenlinien und 
Schattierungen

### Anleitungen

* [OpenTopoMap](https://github.com/der-stefan/OpenTopoMap/blob/master/mapnik/README.md) — Setting up a OpenTopoMap tile server - a complete walkthrough

* [Klas Karlsson on YouTube](https://www.youtube.com/watch?v=g2tVai2Ubzo) — QgisUser0004 - Military Maps

## Lizenz

Kartendaten: © [OpenStreetMap](https://openstreetmap.org/copyright)-Mitwirkende, [SRTM](https://www2.jpl.nasa.gov/srtm/) | Kartendarstellung: © [OpenTopoMap](http://opentopomap.org/) ([CC-BY-SA](https://creativecommons.org/licenses/by-sa/3.0/))
